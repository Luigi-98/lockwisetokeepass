import pandas as pd
from pykeepass import PyKeePass, create_database
import json

kp: PyKeePass = create_database('/home/zucchetti/Documents/LP/db.kdbx', password='pwd')


# add_entry(destination_group, title, username, password, url=None, notes=None, expiry_time=None, tags=None, otp=None, icon=None, force_creation=False)

df = pd.read_csv("logins.csv")
df.username = df.username.fillna('')

for _, row in df.iterrows():
    url = row['url']
    user = row['username']
    pwd = row['password']
    created = row['timeCreated']
    info = {
            key: row[key]
            for key in [
                'httpRealm',
                'formActionOrigin',
                'guid',
                'timeCreated',
                'timeLastUsed',
                'timePasswordChanged'
            ]
    }
    kp.add_entry(
            kp.groups[0],
            title=url.split("://", 1)[-1],
            username=user,
            password=pwd,
            url=url,
            notes=json.dumps(info),
            force_creation=True,
            # created=created?
    )
kp.save()

