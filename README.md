# Preconfiguration
```shell
$ pip install -r requirements.txt
```

# Usage
1. Export logins from Firefox:
	1. from Firefox, open url `about:logins';
	2. three dots -> "Export logins";
	3. save them to this folder as `logins.csv`
2. Run the `translate.py` script
3. A `db.kdbx` file will be generated. It will be a valid KeePass db, that uses "pwd" as password. (You gotta change it asap)

